import React, { Component } from "react";
import HomeBook from "./HomeBook.js";
import { getAll } from "./books.model";
import { Link } from "react-router-dom";

/**
 * Books class
 *
 * create books for home page + add book button
 * @param books {array} list of books from api
 *
 */
class Books extends Component {
  constructor() {
    super();
    this.state = {
      books: []
    };
  }

  componentWillMount() {
    getAll(data => {
      this.setState({ books: data });
    });
  }

  render() {
    return (
      <div>
        <Link to="/add-book">
          <button id="add-book-btn">Add new Book</button>
        </Link>

        <div className="books-container">
          {this.state.books.map(book => (
            <HomeBook data={book} key={book._id} />
          ))}
        </div>
      </div>
    );
  }
}

export default Books;
