import axios from "axios";

/**
 * Books model
 *
 * get/set data from/to api
 *
 * all the functions must get callback, otherwise nothing
 * will display to client
 *
 * some of them get also another param to pass to api
 *
 * @param cb {function} callback function *
 */

const apiUrl = "https://totango-api.herokuapp.com"; //only for this test, otherwise it will store in env variable like proccess.env.API_URL

// get all books
export function getAll(cb) {
  axios
    .get(apiUrl)
    .then(function(response) {
      if (cb) {
        cb(response.data);
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}

// get specific book by id
export function getBookById(bookId, cb) {
  axios
    .get(apiUrl + "/book/" + bookId)
    .then(function(response) {
      if (cb) {
        cb(response.data);
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}

// add new book
export function addNewBook(bookData, cb) {
  axios
    .post(apiUrl + "/add-new-book/", {
      title: bookData.title,
      description: bookData.description,
      isbn: bookData.isbn,
      author: bookData.author,
      pubDate: bookData.pubdate,
      genre: bookData.genre,
      price: bookData.price
    })
    .then(function(response) {
      if (cb) {
        cb(response.data);
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}

// remove book by id
export function removeBook(bookId, cb) {
  axios
    .post(apiUrl + "/delete-book/" + bookId)
    .then(function(response) {
      if (cb) {
        cb(response.data);
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}
