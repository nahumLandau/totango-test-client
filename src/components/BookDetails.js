import React, { Component } from "react";
import BookCover from "../assets/book-cover.png";
import { Redirect } from "react-router-dom";
import { getBookById } from "./books.model";
import { removeBook } from "./books.model";

/**
 * class Book
 *
 * create book details for specific book selected
 * - request book by id from api and display it
 * - if not book with this id, show not found message
 * - remove button
 *
 * @param book {Object} book details
 */
class Book extends Component {
  constructor() {
    super();
    this.state = {
      book: {
        title: "",
        description: "",
        isbn: "",
        author: "",
        pubDate: "",
        genre: "",
        price: ""
      },
      showBook: false,
      bookRemoved: false,
      redirectHome: false
    };
  }
  componentWillMount() {
    getBookById(this.props.match.params.id, data => {
      this.setState({ book: data, showBook: true });
    });
  }

  removeBook() {
    removeBook(this.state.book._id, message => {
      if (!message.error) {
        this.setState({ bookRemoved: true });

        setTimeout(() => {
          this.setState({ redirectHome: true });
        }, 1500);
      }
    });
  }

  /**
   * get date from input and format to readable date
   * @param {String} date in this format "2018-05-30T18:32:30.227Z"
   * @return {String} readable date in this format "30/05/2018"
   */
  getpubDate(date) {
    let onlyDate = date.split("T");
    onlyDate = onlyDate[0];
    let orderDate = onlyDate.split("-");
    return orderDate[2] + "/" + orderDate[1] + "/" + orderDate[0];
  }

  render() {
    if (this.state.book) {
      return (
        <div
          className="book book-details"
          key={this.state.book._id}
          style={{ display: this.state.showBook ? "block" : "none" }}
        >
          <div>
            <div className="book-cover">
              <img src={BookCover} alt=""/>
            </div>

            <h1>{this.state.book.title}</h1>
            <h2>{this.state.book.author}</h2>
            <h3>{this.state.book.price + "$"}</h3>

            <div className="content-block-wrapper">
              <div id="description" className="content-block">
                <h4>Description:</h4>
                {this.state.book.description}
              </div>

              <div id="genre" className="content-block">
                <h4>genre:</h4>
                {this.state.book.genre}
              </div>

              <div id="isbn" className="content-block">
                <h4>ISBN:</h4>
                {this.state.book.isbn}
              </div>

              <div id="pub-date" className="content-block">
                <h4>Publication Date:</h4>
                {this.getpubDate(this.state.book.pubDate)}
              </div>
            </div>

            <button
              id="remove-book"
              onClick={this.removeBook.bind(this)}
              style={{ display: !this.state.bookRemoved ? "block" : "none" }}
            >
              Remove
            </button>
            <div
              className="removed-message"
              style={{ display: this.state.bookRemoved ? "block" : "none" }}
            >
              {'The Book "' +
                this.state.book.title +
                '" was removed successfuly.'}
            </div>
          </div>

          {this.state.redirectHome && <Redirect to="/" />}
        </div>
      );
    } else {
      return (
        <div className="book-not-found">Sorry, nothing to read here...</div>
      );
    }
  }
}

export default Book;
