import React, { Component } from "react";
import BookCover from "../assets/book-cover.png";
import { Route, Link } from "react-router-dom";

/**
 * class Home book
 *
 * display all the books in main page
 * each book has a link to book details page
 */
class HomeBook extends Component {
  render() {
    return (
      <Route>
        <Link to={"/book/" + this.props.data._id}>
          <div className="book">
            <div>
              <div className="book-cover">
                <img src={BookCover} alt="" />
              </div>
              <h1>{this.props.data.title || ""}</h1>
              <h2>
                <span>{this.props.data.author || ""}</span> /{" "}
                {this.props.data.genre || ""}
              </h2>
              <h3>{(this.props.data.price || "") + "$"}</h3>
            </div>
          </div>
        </Link>
      </Route>
    );
  }
}

export default HomeBook;
