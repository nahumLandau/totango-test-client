import React, { Component } from "react";
import { addNewBook } from "./books.model";
import _find from "lodash.find";
import { Redirect } from "react-router-dom";

/**
 * class Add Book
 *
 * - create form for all book fields
 * - validate inputs (for this test, only check if input are empty)
 * - send form after validation to api
 */
class AddBook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {
        title: "",
        description: "",
        author: "",
        pubdate: "",
        isbn: "",
        genre: "",
        price: ""
      },
      isvalidForm: true,
      bookAdded: false,
      redirectHome: false
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    let target = event.target;
    let value = target.value;
    let name = target.name;

    let stateTmp = this.state.inputs;
    stateTmp[name] = value;

    this.setState({
      inputs: stateTmp
    });
  }

  sendBook() {
    let emptyInput = _find(this.state.inputs, input => input === "");
    if (emptyInput === "") {
      this.setState({ isvalidForm: false });
    } else {
      addNewBook(this.state.inputs, response => {
        if (!response.error) {
          this.setState({ bookAdded: true, isvalidForm: true });

          setTimeout(() => {
            this.setState({ redirectHome: true });
          }, 1500);
        } else {
          this.setState({ isvalidForm: false });
        }
      });
    }
  }

  render() {
    return (
      <div className="add-book-container">
        <h2>Add New Book</h2>

        <input
          type="text"
          name="title"
          value={this.state.inputs.title}
          id="add-title"
          placeholder="Title"
          onChange={this.handleInputChange}
        />
        <input
          type="text"
          name="description"
          value={this.state.inputs.description}
          id="add-description"
          placeholder="Description"
          onChange={this.handleInputChange}
        />
        <input
          type="text"
          name="author"
          value={this.state.inputs.authortle}
          id="add-author"
          placeholder="Author"
          onChange={this.handleInputChange}
        />
        <input
          type="date"
          name="pubdate"
          value={this.state.inputs.pubdate}
          id="add-pubdate"
          placeholder="Publication Date"
          onChange={this.handleInputChange}
        />
        <input
          type="tel"
          name="isbn"
          value={this.state.inputs.isbn}
          id="add-isbn"
          placeholder="ISBN"
          onChange={this.handleInputChange}
        />
        <select
          name="genre"
          value={this.state.inputs.genre}
          id="add-genre"
          onChange={this.handleInputChange}
        >
          <option value="">Genre</option>
          <option value="Science fiction">Science fiction</option>
          <option value="Satire">Satire</option>
          <option value="Drama">Drama</option>
          <option value="Action">Action</option>
          <option value="Romance">Romance</option>
          <option value="Mystery">Mystery</option>
          <option value="Horror">Horror</option>
        </select>
        <input
          type="tel"
          name="price"
          value={this.state.inputs.price}
          id="add-price"
          placeholder="Price"
          onChange={this.handleInputChange}
        />

        <div
          className="error"
          style={{ display: this.state.isvalidForm ? "none" : "block" }}
        >
          Please fill all the fields correctly
        </div>

        <button
          id="send-new-book"
          onClick={this.sendBook.bind(this)}
          style={{ display: !this.state.bookAdded ? "block" : "none" }}
        >
          Save
        </button>

        <div
          className="added-message"
          style={{ display: this.state.bookAdded ? "block" : "none" }}
        >
          {'The Book "' + this.state.inputs.title + '" was added successfuly.'}
        </div>

        {this.state.redirectHome && <Redirect to="/" />}
      </div>
    );
  }
}

export default AddBook;
