import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import "./App.css";
import Books from "./components/Books";
import BookDetails from "./components/BookDetails";
import AddBook from "./components/AddBook";

class App extends Component {
  render() {
    return (
      <div>
        <div className="app-bg" />
        <h1 className="app-title">The Totango Bookstore</h1>

        {/* routing */}
        <Router>
          <div>
            <Route exact path="/" component={Books} />
            <Route exact path="/add-book" component={AddBook} />
            <Route path="/book/:id" component={BookDetails} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
